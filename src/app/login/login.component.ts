import { CommonModule } from '@angular/common';
import { Component,inject,TemplateRef } from '@angular/core';
import { ToastService } from '../employee/index/toast-service';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { ToastsContainer } from '../employee/index/toast-container.component';
import { NgbToastModule } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-login',
  standalone: true,
  imports: [CommonModule,ReactiveFormsModule,ToastsContainer,NgbToastModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {
  toastService = inject(ToastService);
  form!:FormGroup;
  birthdatevalue = '';
  failed = false;

  constructor( private authService : AuthService, private router: Router) {

  }

  ngOnInit():void{
    this.form = new FormGroup({
      username: new FormControl('',Validators.required),
      password: new FormControl('',Validators.required),
    })
    if(this.authService.isAuthenticatedUser()){
      this.router.navigate(['/employee']);
    }
  }
  get f(){
    return this.form.controls;
  }
  submit() {
    if (this.form.valid) {
      // console.log(this.form.value.username);
      const username = this.form.value.username;
      const password = this.form.value.password;

       // Call the authentication service's login method
       if (this.authService.login(username, password)) {
            // Navigate to the Employee upon successful login
            // this.router.navigate(['/employee']);
            window.location.href="/employee"
        } else {
            // Handle authentication error (show error message, etc.)
            this.failed = true;
            setTimeout(() => (this.failed = true), 1500);
        }
    }
  }
}
