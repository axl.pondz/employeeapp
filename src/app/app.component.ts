import { Component } from '@angular/core';
import { RouterModule,RouterOutlet,Router } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from './services/auth.service';
import { CommonModule } from '@angular/common';
import { LocalStorageService } from './local-storage.service';


@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule,RouterOutlet,NgbModule,RouterModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'taskfrontweb';
  isCollapsed = true;
  isLogin = false;


  constructor(private authService: AuthService, private router : Router,private localStorageService: LocalStorageService) {}

  ngOnInit():void{
    this.isLogin = this.authService.isAuthenticatedUser();
    // this.localStorageService.clear();
    this.localStorageService.setItem('itemsPerPage','4');
    this.localStorageService.setItem('groupFilter','');
    this.localStorageService.setItem('statusFilter','');
    this.localStorageService.setItem('orderBy','');
    this.localStorageService.setItem('search','');
    // console.log(this.isLogin);
  }

  logout(): void {
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}
