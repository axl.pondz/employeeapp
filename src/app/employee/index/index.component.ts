import { CommonModule } from '@angular/common';
import { Component,inject, OnDestroy, TemplateRef  } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Employee } from '../employee';
import { EmployeeService } from '../employee.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastService } from './toast-service';
import { ToastsContainer } from './toast-container.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { LocalStorageService } from '../../local-storage.service';
import { FormsModule } from '@angular/forms';


@Component({
  selector: 'app-index',
  standalone: true,
  imports: [CommonModule,RouterModule,NgbModule,NgbTooltipModule,ToastsContainer,NgxPaginationModule,FormsModule],
  templateUrl: './index.component.html',
  styleUrl: './index.component.css'
})
export class IndexComponent {
  active = 1;
  toastService = inject(ToastService);
  employees:Employee[]=[];
  stateupdate = false;
  statedelete = false;
  page: number = 1;
  passenger: any; 
  itemsPerPage = 4;
  totalItems : any; 
  showperpage : number = 12;
  groupFilter: string | null ="";
  statusFilter: string | null ="";
  orderBy: string | null ="";
  search: string | null ="";

  constructor(public employeService:EmployeeService,private localStorageService: LocalStorageService){}
  ngOnInit():void{
    this.getData();
  }
  saveToLocalStorage(name:string,value:string) {
    this.localStorageService.setItem(name, value);
  }

  retrieveFromLocalStorage(key:string) {
    const value = this.localStorageService.getItem(key);
    // console.log(value);
    return value;
  }
  getData(){
      this.itemsPerPage = Number(this.retrieveFromLocalStorage('itemsPerPage')) | 4;
      this.groupFilter = this.retrieveFromLocalStorage('groupFilter');
      this.statusFilter = this.retrieveFromLocalStorage('statusFilter');
      this.orderBy = this.retrieveFromLocalStorage('orderBy');
      this.search = this.retrieveFromLocalStorage('search');

      this.employeService.getData(this.page,this.itemsPerPage,this.groupFilter,this.statusFilter,this.orderBy,this.search).subscribe((data:any)=>{
        // console.log(data);
        // new version 
        // this.employees = data.data;
        // this.totalItems = data.items;
        // old version
        this.employees = data;
      })
      // old version count items
      this.employeService.getAll(this.groupFilter,this.statusFilter,this.orderBy,this.search).subscribe((data:any)=>{
        this.totalItems = data.length;
      })
  }
  groupChange(obj: any){
    this.saveToLocalStorage('groupFilter',obj.value);
    this.groupFilter = obj.value;
    this.filterData();
  }
  statusChange(obj: any){
    this.saveToLocalStorage('statusFilter',obj.value);
    this.statusFilter = obj.value;
    this.filterData();
  }
  orderChange(obj: any){
    this.saveToLocalStorage('orderBy',obj.value);
    this.orderBy = obj.value;
    this.filterData();
  }
  searchChange(obj: any){
    this.saveToLocalStorage('search',obj.value);
    this.search = obj.value;
    this.filterData();
  }
  filterData(){
    this.page = 1;
    // console.log(perpage.value);
    this.getData();
  }
  showData(obj: any){
    this.saveToLocalStorage('itemsPerPage',obj.value);
    this.itemsPerPage = obj.value;
    this.page = 1;
    // console.log(perpage.value);
    this.getData();
  }
  getInitials(firstName:string, lastName:string) {
    return firstName[0].toUpperCase() + lastName[0].toUpperCase();
  }
  update(template: TemplateRef<any>){
    this.stateupdate = true;
    this.toastService.show({ template, classname: 'bg-warning', delay: 2000 });
  }
  delete(template: TemplateRef<any>){
    this.statedelete = true;
    console.log("delete");
    this.toastService.show({ template, classname: 'bg-danger text-light', delay: 2000 });
  }
  gty(page: any){
    this.page = page;
    this.getData();
  }
  ngOnDestroy(): void {
		this.toastService.clear();
	}

  
}
