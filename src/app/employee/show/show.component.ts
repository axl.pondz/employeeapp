import { CommonModule } from '@angular/common';
import { EmployeeService } from '../employee.service';
import { Component } from '@angular/core';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Employee } from '../employee';

@Component({
  selector: 'app-show',
  standalone: true,
  imports: [CommonModule,RouterModule,NgbModule],
  templateUrl: './show.component.html',
  styleUrl: './show.component.css'
})
export class ShowComponent {

  id!:number;
  employee!:Employee;

  constructor(public employeService:EmployeeService,private router:Router, private route:ActivatedRoute){}
  ngOnInit():void{
    this.id = this.route.snapshot.params['employeeId'];
    this.getData();
  }

  getData(){
    this.employeService.find(this.id).subscribe((data:any)=>{
      // console.log(data);
      this.employee = data;
    })
  }
}
