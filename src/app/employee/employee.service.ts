import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable,catchError,retry,throwError} from 'rxjs';
import { Employee } from './employee';

@Injectable({
  providedIn: 'root'
})

export class EmployeeService {

  private apiURL = 'http://localhost:3000'
  httpOptions={
    headers:new HttpHeaders({
      'Conten-Type':'aplication/json'
    })
  }
  constructor(private htppClient:HttpClient) { }

  getData(page:number,itemsPerPage:number,groupFilter:string|null,statusFilter:string|null,orderBy:string|null,search:string|null):Observable<any>{
      let url;
      url = this.apiURL+'/employee?_page='+page+'&_limit='+itemsPerPage;

      if(groupFilter){
        url = url+'&group='+groupFilter;
      }
      if(statusFilter){
        url = url+'&status='+statusFilter;
      }
      if(orderBy){
        url = url+'&_sort='+orderBy;
      }
      if(search){
        url = url+'&q='+search;
      }
      return this.htppClient.get(url).pipe(catchError((error:HttpErrorResponse) => {
        return throwError(error)
      })
    )
  }

  getAll(groupFilter:string|null,statusFilter:string|null,orderBy:string|null,search:string|null):Observable<any>{
      let url;
      url = this.apiURL+'/employee?nothing=1';

      if(groupFilter){
        url = url+'&group='+groupFilter;
      }
      if(statusFilter){
        url = url+'&status='+statusFilter;
      }
      if(orderBy){
        url = url+'&_sort='+orderBy;
      }
      if(search){
        url = url+'&q='+search;
      }
      return this.htppClient.get(url).pipe(catchError((error:HttpErrorResponse) => {
        return throwError(error)
      })
    )
  }

  create(employee:Employee):Observable<any>{
    return this.htppClient.post(this.apiURL+'/employee',employee,this.httpOptions).pipe(catchError((error:HttpErrorResponse) => {
      return throwError(error);
    }))
  }

  find(id:number):Observable<any>{
    return this.htppClient.get(this.apiURL+'/employee/'+id).pipe(catchError((error:HttpErrorResponse) => {
      return throwError(error);
    }))
  }

}
