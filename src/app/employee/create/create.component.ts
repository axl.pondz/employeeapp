import { CommonModule } from '@angular/common';
import { Component,inject } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { EmployeeService } from '../employee.service';
import { Router, RouterModule } from '@angular/router';
import { NgbModule,NgbCalendar, NgbDatepickerModule, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { JsonPipe } from '@angular/common';
import { Select2Data, Select2Module } from 'ng-select2-component';
import { NgxCurrencyDirective } from "ngx-currency";

@Component({
  selector: 'app-create',
  standalone: true,
  imports: [CommonModule,ReactiveFormsModule,NgbModule,NgbDatepickerModule, FormsModule, JsonPipe, Select2Module,RouterModule,NgxCurrencyDirective],
  templateUrl: './create.component.html',
  styleUrl: './create.component.css'
})
export class CreateComponent {
    form!:FormGroup;
    reform:any;
    birthdatevalue = '';

    constructor(public employeeService:EmployeeService, private router:Router){

    }

  ngOnInit():void{
      this.form = new FormGroup({
        firstName: new FormControl('',Validators.required),
        lastName: new FormControl('',Validators.required),
        email: new FormControl('',Validators.required),
        username: new FormControl('',Validators.required),
        basicSalary: new FormControl('',Validators.required),
        birthDate: new FormControl('',Validators.required),
        group: new FormControl('',Validators.required),
        description: new FormControl('',Validators.required)
        // status: new FormControl('',Validators.required),
    })
  }

  get f(){
    return this.form.controls;
  }

  submit(){
    console.log(this.form.value);
    this.reform = {
      id: null,
      username: this.form.value.username,
      firstName: this.form.value.firstName,
      lastName: this.form.value.lastName,
      email: this.form.value.email,
      birthDate: this.form.value.birthDate.year +'-'+ this.pad(this.form.value.birthDate.month,2)+'-'+this.pad(this.form.value.birthDate.day,2),
      basicSalary: this.form.value.basicSalary,
      status: "Active",
      group: this.form.value.group,
      description: this.form.value.description
    }
    // console.log(this.reform);
    this.employeeService.create(this.reform).subscribe((res:any)=>{
      alert("Data Has Successfully Created!");
      this.router.navigateByUrl('employee/index');
    })
  }
  select2update(obj:any){
    // this.select2value = obj.value;
    // console.log(obj.value);
    // this.select2valueid = obj.value
  }

  onDateSelect(obj:any){
    // console.log(obj);
    // this.birthdatevalue = obj.year + "-" + this.pad(obj.month,2) + "-" + this.pad(obj.day,2);
    // console.log(this.today);
  }

  pad(num:number, size:number): string {
      let s = num+"";
      while (s.length < size) s = "0" + s;
      return s;
  }

  today = inject(NgbCalendar).getToday();
	// model!: NgbDateStruct;
	maxDate=this.today;
	date!: { year: number; month: number };
  select2data: Select2Data = [
      {
          value: 'Frontend Development',
          label: 'Frontend Development',
      },
      {
          value: 'Backend Development',
          label: 'Backend Development',
      },
      {
        value: 'Scrum Master',
        label: 'Scrum Master',
      },
      {
        value: 'Fullstack Development',
        label: 'Fullstack Development',
      },
      {
        value: 'System Analyst',
        label: 'System Analyst',
      },
      {
        value: 'Data Analyst',
        label: 'Data Analyst',
      },
      {
        value: 'Quality Asurance',
        label: 'Quality Asurance',
      },
      {
        value: 'Database Administrator',
        label: 'Database Administrator',
      },
      {
        value: 'DevOps',
        label: 'DevOps',
      },
  ];
  select2value = [{value: '', label: 'Selected Value'}];
  select2placeholder = "Selected Value";
  select2valueid = "";
}
