import { Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { IndexComponent } from './employee/index/index.component';
import { CreateComponent } from './employee/create/create.component';
import { ShowComponent } from './employee/show/show.component';
import { LoginComponent } from './login/login.component';


export const routes: Routes = [
    {path:'',redirectTo:'login', pathMatch:'full'},
    {path:'login', component:LoginComponent},
    {path:'employee',redirectTo:'employee/index', pathMatch:'full'},
    {path:'employee/index',component:IndexComponent,canActivate: [AuthGuard]},
    {path:'employee/create',component:CreateComponent,canActivate: [AuthGuard]},
    {path:'employee/:employeeId/show',component:ShowComponent,canActivate: [AuthGuard]},
    {path: '**', redirectTo: '/login'},
];
