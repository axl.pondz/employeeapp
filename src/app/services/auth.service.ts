import { Injectable } from '@angular/core';
import { LocalStorageService } from '../local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private isAuthenticated = false;
  private authSecretKey = 'Bearer Token';

  constructor(private localStorageService: LocalStorageService) { 
    this.isAuthenticated = !!localStorage.getItem(this.authSecretKey);
  }
  
  login(username: string, password: string): boolean {
    console.log(username,password);
    if (username === 'adipanca' && password === 'adipanca2024') {
      const authToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6ImFkaXBhbmNhIiwiaWF0IjoxNTE2MjM5MDIyfQ.Rey3SpKfQZE6O_7iT3T_yBjMwPfTfBAMlgmGrZ0icyo'; // Generate or receive the token from your server
      localStorage.setItem(this.authSecretKey, authToken);
      this.isAuthenticated = true;
      return true;
    } else {
      return false;
    }
  }

  isAuthenticatedUser(): boolean {
    return this.isAuthenticated;
  }

  logout(): void {
    localStorage.removeItem(this.authSecretKey);
    localStorage.removeItem('itemsPerPage');
    localStorage.removeItem('groupFilter');
    localStorage.removeItem('statusFilter');
    localStorage.removeItem('orderBy');
    localStorage.removeItem('search');
    this.localStorageService.clear();
    this.isAuthenticated = false;
  }
}