# employeeapp
Angular Version 17.x
json-server 0.17.4


## Getting started
[HOW TO USE]
before you run the all step, please check your node version as lastest version.
1. git clone https://gitlab.com/axl.pondz/employeeapp.git
2. install Angular CLI npm install -g @angular/cli
3. open cmd and run npm install
4. open cmd and run ng serve
5. open cmd and run npx json-server db.json
6. open browser and access url from point 4. (default url is http://localhost:4200/)
7. at the browser you must be fill the username and password (use => username : adipanca | password : adipanca2024)

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project and thank you for Angular and json-server.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
